import arcade.key

from random import randint


GAME_RUNNING = 0
GAME_OVER = 1


class Model:

    def __init__(self, world, x, y, angle):
        self.world = world
        self.x = x
        self.y = y
        self.angle = 0

    def hit(self, other, hit_size):
        return (abs(self.x - other.x) <= hit_size) and (abs(self.y - other.y) <= hit_size)

        
class Monster:

    STATE_ALIVE = 0

    def __init__(self, x, y, width, height, direction):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.is_died = False
        self.state = self.STATE_ALIVE
        self.direction = direction
        self.timeToAttack = randint(7, 12)
        self.timePass = 0;

    def shot(self,i,j):
        return ((abs(self.x - i) < 50) and (abs(self.y - j) < 50))


class World:

    def __init__(self, width, height):

        self.width = width
        self.height = height
        self.current_state = GAME_RUNNING
        self.monster1 = []
        self.monster2 = []
        self.numberEnermy = 2
        for i in range(1):
            self.monster1.append(Monster( self.width/3, self.height/2, 100, 100, randint(0, 3)))
            self.monster2.append(Monster( self.width*2/3, self.height/2, 100, 100, randint(0, 3)))

        self.number = 1
        self.score = 0
        self.gameTime = 0
        self.spawn_time = 0
        self.spawncount_time = 0
        self.lifePoint = 3
        self.numberOfTypeMonster = 1
        self.missPoint = 5


    def spawnMonster(self):
        if self.gameTime - self.spawncount_time > 10:
            self.number += 1
            self.spawncount_time = self.gameTime
        if self.gameTime - self.spawn_time > 1 + (50/self.number):
            self.spawn_time = self.gameTime
            if(randint(1, 2)%2 == 0):
                for i in range(1):
                    self.monster1.append(Monster(randint(100, 1180), randint(200, 620), 100, 100, randint(0, 3)))
                    self.numberEnermy += 1
            else:
                for i in range(1):
                    self.monster2.append(Monster(randint(100, 1180), randint(200, 620), 100, 100, randint(0, 3)))
                    self.numberEnermy += 1

        if self.number > self.numberEnermy:
            if(randint(1, 2)%2 == 0):
                for i in range(1):
                    self.monster1.append(Monster(randint(100, 1180), randint(200, 620), 100, 100, randint(0, 3)))
                    self.numberEnermy += 1
            else:
                for i in range(1):
                    self.monster2.append(Monster(randint(100, 1180), randint(200, 620), 100, 100, randint(0, 3)))
                    self.numberEnermy += 1


    def animate(self, delta_time):
        self.spawnMonster()
        self.gameTime += delta_time
        self.takeDamage()


    def addScore(self, num):
        if self.score + num < 0:
            self.score == 0
        else:
            self.score += num


    def takeDamage(self):
        for m in self.monster1:
            if self.gameTime - m.timePass > 1:
                m.timeToAttack -= 1
                m.timePass = self.gameTime
            if m.timeToAttack <= 0:
                if self.lifePoint <= 0:
                    self.current_state = GAME_OVER
                else:
                    self.lifePoint -= 1
                    m.timeToAttack = 10
        for n in self.monster2:
            if self.gameTime - n.timePass > 1:
                n.timeToAttack -= 1
                n.timePass = self.gameTime
            if n.timeToAttack <= 0:
                if self.lifePoint <= 0:
                    self.current_state = GAME_OVER
                else:
                    self.lifePoint -= 1
                    n.timeToAttack = 10


    def on_mouse_release(self, x, y, button, modifiers):
        if button == arcade.MOUSE_BUTTON_LEFT:
            for m in self.monster1:
                if((not m.is_died) and m.shot(x,y)):
                    self.monster1.remove(m)
                    self.numberEnermy -= 1
                    self.addScore(10)
            for n in self.monster2:
                if((not n.is_died) and n.shot(x,y)):
                    self.missTarget()


        if button == arcade.MOUSE_BUTTON_RIGHT:
            for n in self.monster2:
                if((not n.is_died) and n.shot(x,y)):
                    self.monster2.remove(n)
                    self.numberEnermy -= 1
                    self.addScore(10)
            for m in self.monster1:
                if((not m.is_died) and m.shot(x,y)):
                    self.missTarget()


    def missTarget(self):
        if self.missPoint == 0:
            self.missPoint = 5
            self.lifePoint -= 1
        else:
            self.missPoint -= 1