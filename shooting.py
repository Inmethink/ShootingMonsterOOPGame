import arcade

from models import World, Monster


SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

GAME_RUNNING = 0
GAME_OVER = 1



class ModelSprite(arcade.Sprite):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', None)
        super().__init__(*args, **kwargs)

    def sync_with_model(self):
        if self.model:
            self.set_position(self.model.x, self.model.y)
            self.angle = self.model.angle

    def draw(self):
        self.sync_with_model()
        super().draw()


class ShootingEnermyGame(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.high_score = 0
        self.setup(width, height)
        self.player = None
        self.left_down = False
        self.timeClick = 0
        self.firstTimeMonster = True


    def setup(self, width, height):
        arcade.set_background_color(arcade.color.BLACK)
        self.world = World(width, height)
        self.background_texture = arcade.load_texture('Picture/backgroundTest.png')
        self.monster1_texture = arcade.load_texture('Picture/Monster2.png')
        self.monster2_texture = arcade.load_texture('Picture/Monster3.png')
        self.gameover_texture = arcade.load_texture('Picture/Gameover.png')

        
    def draw_monster1(self, monster1):
        for m in monster1:
           if m.is_died == False:
               arcade.draw_texture_rectangle(m.x, m.y, m.width, m.height, self.monster1_texture)
               arcade.draw_text(str(m.timeToAttack), m.x - 7 , m.y + 55 , arcade.color.WHITE, 20)


    def draw_monster2(self, monster2):
        for n in monster2:
           if n.is_died == False:
               arcade.draw_texture_rectangle(n.x, n.y, n.width, n.height, self.monster2_texture)
               arcade.draw_text(str(n.timeToAttack), n.x - 7 , n.y + 55 , arcade.color.WHITE, 20)


    def animate(self, delta_time):

        if self.world.current_state == GAME_RUNNING:
            self.world.animate(delta_time)


    def on_draw(self):
        arcade.start_render()
        arcade.draw_texture_rectangle(640, 360, 1280, 720, self.background_texture)
        self.draw_monster1(self.world.monster1)
        self.draw_monster2(self.world.monster2)
        arcade.draw_text(str("LifePoint: "), self.width - 1100, self.height - 30, arcade.color.WHITE, 20)
        arcade.draw_text(str(self.world.lifePoint), self.width - 980, self.height - 30, arcade.color.WHITE, 20)
        arcade.draw_text(str("Score: "), self.width - 800, self.height - 30, arcade.color.WHITE, 20)
        arcade.draw_text(str(self.world.score), self.width - 700, self.height - 30, arcade.color.WHITE, 20)
        arcade.draw_text(str("MissPoint: "), self.width - 540, self.height - 30, arcade.color.WHITE, 20)
        arcade.draw_text(str(self.world.missPoint), self.width - 420, self.height - 30, arcade.color.WHITE, 20)
        if self.firstTimeMonster == True:
        	arcade.draw_text(str("LEFT-CLICK"), self.width/3, self.height/2 - 65, arcade.color.RED, 20)
        	arcade.draw_text(str("RIGHT-CLICK"), self.width*2/3 , self.height/2 - 65, arcade.color.RED, 20)
        	if self.world.score >= 20:
        		self.firstTimeMonster = False

        if self.world.current_state == GAME_OVER:
        	arcade.draw_texture_rectangle( self.width/2, self.height/2, self.width, self.height, self.gameover_texture)
        	arcade.draw_text(str("Your score: "), self.width/2 - 100, self.height - 690, arcade.color.WHITE, 20)
        	arcade.draw_text(str(self.world.score), self.width/2 + 30, self.height - 690, arcade.color.WHITE, 20)


    def on_mouse_release(self, x, y, button, modifiers):
    	if self.world.current_state == GAME_RUNNING:
    		self.world.on_mouse_release(x, y, button, modifiers)

    	elif self.world.current_state == GAME_OVER:
    		self.timeClick += 1
    		if self.timeClick >= 5:
    			self.setup(1280, 720)

if __name__ == '__main__':
    window = ShootingEnermyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.run()
